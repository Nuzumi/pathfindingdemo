using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

namespace Assets.Scripts.Character
{
    public class CharacterController : MonoBehaviour
    {
        [SerializeField] private float moveSpeed;

        private List<Vector3> path;

        private Vector3 nextPosition;
        private Vector3 currentPosition;
        private int currentPositionIndex;
        private float movePercent;
        private bool move = false;

        public void WalkThePath(List<Vector3> path)
        {
            if (path.Count == 0)
            {
                move = false;
                return;
            }

            move = true;
            var nextPositionIndex = path.IndexOf(nextPosition);

            if (nextPositionIndex != -1)
            {
                currentPositionIndex = nextPositionIndex - 1;
            }
            else
            {
                currentPositionIndex = 0;
                currentPosition = path[currentPositionIndex];
                nextPosition = path[currentPositionIndex + 1];
                transform.position = currentPosition;
            }

            this.path = path;
        }

        private void Update()
        {
            if (!move)
            {
                return;
            }

            movePercent += Time.deltaTime * moveSpeed;

            if (movePercent > 1)
            {
                UpdatePositionsState();
                movePercent = 0;
                return;
            }

            transform.position = math.lerp(currentPosition, nextPosition, movePercent);
        }

        private void UpdatePositionsState()
        {
            if (currentPositionIndex + 2 >= path.Count)
            {
                move = false;
                return;
            }

            currentPositionIndex++;
            currentPosition = nextPosition;
            nextPosition = path[currentPositionIndex + 1];
        }
    }
}