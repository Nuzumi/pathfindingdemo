﻿using System;

namespace Assets.Scripts.PathFinding
{
    public struct Node : IComparable<Node>
    {
        public int index;
        public int previousIndex;
        public float distanceSqr;

        public int CompareTo(Node other) => distanceSqr.CompareTo(other.distanceSqr);
    }
}
