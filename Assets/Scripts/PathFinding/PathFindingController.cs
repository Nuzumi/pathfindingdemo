﻿using Assets.Scripts.Grid;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;

namespace Assets.Scripts.PathFinding
{

    public class PathFindingController : MonoBehaviour
    {
        private const int NeighboursCount = 4;
        private const int NoNeighbourIndex = -1;

        [SerializeField] private GridConfig gridConfig;
        [SerializeField] private ObstacleGridController obstacleGridController;

        private NativeArray<int> neighbours;

        public List<Vector3> GetPath(int startIndex, int endIndex)
        {
            if (!obstacleGridController.Grid[endIndex])
            {
                return new List<Vector3>();
            }

            var path = new NativeList<int>(Allocator.TempJob);

            new PathFindingJob
            {
                traversableGrid = obstacleGridController.Grid,
                neighbours = neighbours.AsReadOnly(),
                startIndex = startIndex,
                endIndex = endIndex,
                cellNeighbourCount = NeighboursCount,
                gridSize = new int2(gridConfig.GridSize.x, gridConfig.GridSize.y),
                path = path,
            }
            .Schedule()
            .Complete();

            var result = new List<Vector3>();

            for(int i = path.Length - 1; i >= 0; i--)
            {
                result.Add(gridConfig.GetIndexPosition(path[i]));
            }

            path.Dispose();
            return result;
        }

        private void Start()
        {
            CreateNeighbourArray();
        }

        private void OnDestroy()
        {
            neighbours.Dispose();
        }

        private void CreateNeighbourArray()
        {
            neighbours = new NativeArray<int>(gridConfig.CellsCount * NeighboursCount, Allocator.Persistent);

            Vector2Int[] directions = new Vector2Int[]
            {
                new(0, 1),
                new(1, 0),
                new(0, -1),
                new(-1, 0)
            };

            int width = gridConfig.GridSize.x;

            for (int i = 0; i < gridConfig.CellsCount; i++)
            {
                var position = new Vector2Int(i % width, i / width);

                for (int j = 0; j < NeighboursCount; j++)
                {
                    var neighbourPosition = position + directions[j];

                    if (gridConfig.IsPositionOnGrid(neighbourPosition))
                    {
                        var neighbourIndex = neighbourPosition.y * width + neighbourPosition.x;
                        neighbours[i * NeighboursCount + j] = neighbourIndex;
                    }
                    else
                    {
                        neighbours[i * NeighboursCount + j] = NoNeighbourIndex;
                    }
                }
            }
        }

    }
}
