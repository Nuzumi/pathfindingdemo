﻿using System;
using Unity.Collections;

namespace Assets.Scripts.PathFinding
{
    public struct NativePriorityQueue<T> : IDisposable where T : unmanaged, IComparable<T>
    {
        public readonly int Length => values.Length;

        private NativeList<T> values;

        public NativePriorityQueue(int initialCapacity)
        {
            values = new NativeList<T>(initialCapacity, Allocator.Temp);
        }

        public void Enqueue(T value)
        {
            if(values.Length == 0)
            {
                values.Add(value);
                return;
            }

            int currentIndex = 0;
            T currentValue = values[0];

            while(value.CompareTo(currentValue) != -1)
            {
                currentIndex++;

                if(currentIndex >= values.Length)
                {
                    values.Add(value);
                    return;
                }

                currentValue = values[currentIndex];
            }

            values.InsertRange(currentIndex, 1);
            values[currentIndex] = value;
        }

        public T Dequeue()
        {
            var value = values[0];
            values.RemoveAt(0);
            return value;
        }

        public void Dispose()
        {
            values.Dispose();
        }
    }
}
