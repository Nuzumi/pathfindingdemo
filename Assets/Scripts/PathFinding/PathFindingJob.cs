﻿using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;

namespace Assets.Scripts.PathFinding
{
    [BurstCompile]
    public struct PathFindingJob : IJob
    {
        private const int NonExistingNeighbour = -1;

        public NativeArray<bool>.ReadOnly traversableGrid;
        public NativeArray<int>.ReadOnly neighbours;
        public int startIndex;
        public int endIndex;
        public int cellNeighbourCount;
        public int2 gridSize;

        [WriteOnly] public NativeList<int> path;

        public void Execute()
        {
            var initialSize = gridSize.x * gridSize.y;
            var queue = new NativePriorityQueue<Node>(initialSize);
            var routes = new NativeHashMap<int, Node>(initialSize, Allocator.Temp);
            var processedNodes = new NativeHashSet<int>(initialSize, Allocator.Temp);
            var targetPosition = GetIndexPosition(endIndex);

            processedNodes.Add(NonExistingNeighbour);

            queue.Enqueue(new Node
            {
                index = startIndex,
                previousIndex = NonExistingNeighbour,
                distanceSqr = 0
            });

            while(queue.Length > 0)
            {
                var currentNode = queue.Dequeue();

                if (processedNodes.Contains(currentNode.index))
                {
                    continue;
                }

                processedNodes.Add(currentNode.index);
                routes[currentNode.index] = currentNode;

                if (currentNode.index == endIndex)
                {
                    break;
                }

                EnqueueNeighbours(ref queue, ref processedNodes, targetPosition, currentNode);
            }

            if (routes.ContainsKey(endIndex))
            {
                ConstructPath(ref routes);
            }

            queue.Dispose();
            routes.Dispose();
            processedNodes.Dispose();
        }

        private void EnqueueNeighbours(ref NativePriorityQueue<Node> queue, ref NativeHashSet<int> processedNodes, in int2 targetPosition, in Node currentNode)
        {
            var neighbourStartIndex = currentNode.index * cellNeighbourCount;

            for (var i = 0; i < cellNeighbourCount; i++)
            {
                var neighbourIndex = neighbours[neighbourStartIndex + i];

                if (processedNodes.Contains(neighbourIndex))
                {
                    continue;
                }

                if (!traversableGrid[neighbourIndex])
                {
                    continue;
                }

                var node = GetNode(neighbourIndex, currentNode, targetPosition);
                queue.Enqueue(node);
            }
        }

        private Node GetNode(int index, in Node previous, in int2 target)
        {
            var position = GetIndexPosition(index);
            var distnceSqr = math.distancesq(position, target);

            return new Node
            {
                index = index,
                previousIndex = previous.index,
                distanceSqr = distnceSqr + previous.distanceSqr,
            };
        }

        private int2 GetIndexPosition(int index)
        {
            var x = index % gridSize.x;
            var y = index / gridSize.x;
            return new int2(x, y);
        }

        private void ConstructPath(ref NativeHashMap<int, Node> routes)
        {
            var pathNode = routes[endIndex];

            while (pathNode.previousIndex != NonExistingNeighbour)
            {
                path.Add(pathNode.index);
                pathNode = routes[pathNode.previousIndex];
            }

            path.Add(pathNode.index);
        }
    }
}
