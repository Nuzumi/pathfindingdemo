﻿using UnityEngine;

namespace Assets.Scripts.Utils
{
    public static class MathUtils
    {
        public static Vector3Int ToIntVector(this Vector3 vector) => new((int)vector.x, (int)vector.y, (int)vector.z);
    }
}
