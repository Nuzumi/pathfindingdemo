using Assets.Scripts.Grid;
using UnityEngine;

public class GroundController : MonoBehaviour
{
    private const float groundDefaultSize = 10;

    [SerializeField] private GridConfig gridConfig;
    [SerializeField] private Material groundMaterial;

    private void Start()
    {
        SetTransform();
        SetTiling();
    }

    private void SetTransform()
    {
        transform.position = new Vector3(gridConfig.GridSize.x / 2f, 0, gridConfig.GridSize.y / 2f);
        transform.localScale = new Vector3(gridConfig.GridSize.x / groundDefaultSize, 1, gridConfig.GridSize.y / groundDefaultSize);
    }

    private void SetTiling()
    {
        groundMaterial.mainTextureScale = new Vector2(gridConfig.GridSize.x / groundDefaultSize, gridConfig.GridSize.y / groundDefaultSize);
    }
}
