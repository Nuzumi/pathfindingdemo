﻿using Assets.Scripts.Character;
using Assets.Scripts.Grid;
using Assets.Scripts.PathFinding;
using Assets.Scripts.UI.PathComputeTimeDisplay;
using System.Diagnostics;
using UnityEngine;

using CharacterController = Assets.Scripts.Character.CharacterController;

namespace Assets.Scripts.Path
{
    public class PathController : MonoBehaviour
    {
        [SerializeField] private GridConfig gridConfig;
        [SerializeField] private PathFindingController pathFindingController;
        [SerializeField] private PathDisplayController pathDisplayController;
        [SerializeField] private PathComputeTimeDisplayController pathComputeTimeDisplayController;
        [SerializeField] private CharacterController characterController;

        private int pathStartIndex;
        private int pathEndIndex;

        public void SetStartPoint(Vector3 startPoint)
        {
            var startIndex = gridConfig.GetPositionIndex(startPoint);
            if(startIndex == pathStartIndex)
            {
                return;
            }

            pathStartIndex = startIndex;
            CreatePath();
        }

        public void SetEndPoint(Vector3 endPoint)
        {
            var endIndex = gridConfig.GetPositionIndex(endPoint);
            if(endIndex == pathEndIndex)
            {
                return;
            }

            pathEndIndex = endIndex;
            CreatePath();
        }

        public void CreatePath()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var path = pathFindingController.GetPath(pathStartIndex, pathEndIndex);
            stopwatch.Stop();

            pathDisplayController.DisplayPath(path);
            pathComputeTimeDisplayController.SetComputeTime(stopwatch);
            characterController.WalkThePath(path);
        }
    }
}
