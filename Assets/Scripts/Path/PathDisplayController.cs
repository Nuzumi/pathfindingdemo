﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Path
{
    public class PathDisplayController : MonoBehaviour
    {
        [SerializeField] private Vector3 offset;
        [SerializeField] private LineRenderer pathDisplay;

        public void DisplayPath(List<Vector3> positions)
        {
            var array = new Vector3[positions.Count];

            for (int i = 0; i < positions.Count; i++)
            {
                array[i] = positions[i] + offset;
            }

            pathDisplay.positionCount = positions.Count;
            pathDisplay.SetPositions(array);
        }
    }
}
