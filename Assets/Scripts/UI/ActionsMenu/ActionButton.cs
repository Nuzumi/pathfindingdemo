﻿using Assets.Scripts.Input;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.ActionsMenu
{
    public class ActionButton : MonoBehaviour
    {
        [SerializeField] private PlayerAction action;
        [SerializeField] private Button button;

        private Action<PlayerAction> clickAction;

        public void Initialize(Action<PlayerAction> clickAction)
        {
            this.clickAction = clickAction;
        }

        private void Awake()
        {
            button.onClick.AddListener(OnButtonClicked);
        }

        private void OnDestroy()
        {
            button.onClick.RemoveAllListeners();
        }

        private void OnButtonClicked() => clickAction?.Invoke(action);
    }
}