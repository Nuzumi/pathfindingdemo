using Assets.Scripts.Input;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.UI.ActionsMenu
{
    public class ActionsMenuController : MonoBehaviour
    {
        [SerializeField] private List<ActionButton> actionButtons;
        [SerializeField] private PlayerActionsController actionsController;

        private void Start()
        {
            foreach (var button in actionButtons)
            {
                button.Initialize(ActionSelected);
            }
        }

        private void ActionSelected(PlayerAction action) => actionsController.SetActiveAction(action);
    }
}