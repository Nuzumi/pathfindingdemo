using System.Diagnostics;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.UI.PathComputeTimeDisplay
{
    public class PathComputeTimeDisplayController : MonoBehaviour
    {
        private const int TicksToMilisecondsMultiplier = 1000;

        [SerializeField] private TextMeshProUGUI timeText;

        public void SetComputeTime(Stopwatch stopwatch)
        {
            long elapsedTicks = stopwatch.ElapsedTicks;
            double elapsedMilliseconds = (double)elapsedTicks / Stopwatch.Frequency * TicksToMilisecondsMultiplier;
            timeText.text = $"{elapsedMilliseconds} ms";
        }
    }
}