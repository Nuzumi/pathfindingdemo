using Unity.Mathematics;
using UnityEngine;

namespace Assets.Scripts.Input
{
    public class CameraController : MonoBehaviour
    {
        [SerializeField] private float minMoveSpeed;
        [SerializeField] private float maxMoveSpeed;
        [SerializeField] private float zoomSpeed;
        [SerializeField] private float minCameraHeight;
        [SerializeField] private float maxCameraHeight;
        [SerializeField] new private Camera camera;

        private Controlls.CameraMovementActions cameraActions;
        private float zoomedMoveSpeed;

        private void Start()
        {
            cameraActions = new Controlls().CameraMovement;

            cameraActions.Enable();
        }
        private void OnDestroy()
        {
            cameraActions.Disable();
        }

        private void Update()
        {
            CalculateZoomedMoveSpeed();
            MoveCamera();
            ZoomCamera();
        }

        private void CalculateZoomedMoveSpeed()
        {
            var zoomLevel = transform.position.y;
            var zoomPercent = math.unlerp(minCameraHeight, maxCameraHeight, zoomLevel);
            zoomedMoveSpeed = math.lerp(minMoveSpeed, maxMoveSpeed, zoomPercent);
        }

        private void MoveCamera()
        {
            var move = cameraActions.CameraMovement.ReadValue<Vector2>();
            transform.position += new Vector3(move.x, 0, move.y) * (zoomedMoveSpeed * Time.deltaTime);
        }

        private void ZoomCamera()
        {
            var zoom = cameraActions.Zoom.ReadValue<Vector2>().y;

            if (zoom == 0)
            {
                return;
            }

            if (zoom > 0 && transform.position.y < maxCameraHeight)
            {
                ApplyZoom();
            }

            if (zoom < 0 && transform.position.y > minCameraHeight)
            {
                ApplyZoom();
            }

            void ApplyZoom()
            {
                var position = transform.position;
                position.y += zoom * zoomSpeed * Time.deltaTime;
                var clampedHeight = math.min(math.max(position.y, minCameraHeight), maxCameraHeight);
                position.y = clampedHeight;
                transform.position = position;
            }
        }
    }
}