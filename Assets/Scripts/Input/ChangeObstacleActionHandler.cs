﻿using Assets.Scripts.Grid;
using Assets.Scripts.Path;

namespace Assets.Scripts.Input
{
    public class ChangeObstacleActionHandler : PlayerActionHandler
    {
        private readonly ObstacleGridController obstacleGridController;
        private readonly PathController pathController;

        public ChangeObstacleActionHandler(ObstacleGridController obstacleGridController, PathController pathController)
        {
            this.obstacleGridController = obstacleGridController;
            this.pathController = pathController;
        }

        public override void PlayerAction()
        {
            obstacleGridController.ChangeObstacleAtPosition(currentPosition);
            pathController.CreatePath();
        }
    }
}
