﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace Assets.Scripts.Input
{

    public class InteractionsInputController : MonoBehaviour
    {
        public event Action MouseAction;
        public Vector3 MouseWorldPosition { get; private set; }

        private Controlls.InteractionsActions actions;
        private Camera mainCamera;

        private void Start()
        {
            mainCamera = Camera.main;
            actions = new Controlls().Interactions;
            actions.MouseAction.performed += OnMouseActionPerformed;

            actions.Enable();
        }

        private void OnDestroy()
        {
            actions.Disable();
        }

        private void Update()
        {
            MouseWorldPosition = GetMouseWorldPosition();
        }

        private void OnMouseActionPerformed(InputAction.CallbackContext _)
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                return;
            }

            MouseAction?.Invoke();
        }

        private Vector3 GetMouseWorldPosition()
        {
            var mouseScreenPosition = actions.MousePosition.ReadValue<Vector2>();
            var ray = mainCamera.ScreenPointToRay(mouseScreenPosition);
            if(Physics.Raycast(ray, out var hit))
            {
                return hit.point;
            }

            return MouseWorldPosition;
        }

    }
}
