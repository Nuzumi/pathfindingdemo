﻿using UnityEngine;

namespace Assets.Scripts.Input
{
    public abstract class PlayerActionHandler
    {
        protected Vector3 currentPosition;

        public abstract void PlayerAction();

        public virtual void SetMousePosition(Vector3 position)
        {
            currentPosition = position;
        }
    }
}
