﻿namespace Assets.Scripts.Input
{
    public enum PlayerAction
    {
        None,
        ChangeObstacle,
        SetStart,
        SetEnd,
    }
}
