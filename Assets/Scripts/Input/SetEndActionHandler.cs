﻿using Assets.Scripts.Path;
using System;
using UnityEngine;

namespace Assets.Scripts.Input
{
    public class SetEndActionHandler : PlayerActionHandler
    {
        private readonly PathController pathController;
        private readonly Action confirmAction;

        public SetEndActionHandler(PathController pathController, Action confirmAction)
        {
            this.pathController = pathController;
            this.confirmAction = confirmAction;
        }

        public override void PlayerAction()
        {
            confirmAction();
        }

        public override void SetMousePosition(Vector3 position)
        {
            base.SetMousePosition(position);
            pathController.SetEndPoint(position);
        }
    }
}
