﻿using Assets.Scripts.Grid;
using Assets.Scripts.Path;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Input
{

    public class PlayerActionsController : MonoBehaviour
    {
        [SerializeField] private ObstacleGridController gridController;
        [SerializeField] private PathController pathController;
        [SerializeField] private InteractionsInputController interactionsInputController;

        private Dictionary<PlayerAction, PlayerActionHandler> actions;

        private PlayerActionHandler currentActionHandler;

        public void SetActiveAction(PlayerAction action)
        {
            currentActionHandler = actions[action];
        }

        private void Awake()
        {
            actions = new Dictionary<PlayerAction, PlayerActionHandler>
            {
                {PlayerAction.ChangeObstacle, new ChangeObstacleActionHandler(gridController, pathController) },
                {PlayerAction.SetStart, new SetStartActionHandler(pathController, () => SetActiveAction(PlayerAction.ChangeObstacle) )},
                {PlayerAction.SetEnd, new SetEndActionHandler(pathController, () => SetActiveAction(PlayerAction.ChangeObstacle)) },
            };

            currentActionHandler = actions[PlayerAction.ChangeObstacle];
        }

        private void Start()
        {
            interactionsInputController.MouseAction += OnMouseAction;
        }

        private void OnDestroy()
        {
            interactionsInputController.MouseAction -= OnMouseAction;
        }

        private void Update()
        {
            currentActionHandler.SetMousePosition(interactionsInputController.MouseWorldPosition);
        }

        private void OnMouseAction()
        {
            currentActionHandler.PlayerAction();
        }
    }
}
