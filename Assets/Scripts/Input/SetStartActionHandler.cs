﻿using Assets.Scripts.Path;
using System;
using UnityEngine;

namespace Assets.Scripts.Input
{
    public class SetStartActionHandler : PlayerActionHandler
    {
        private readonly PathController pathController;
        private readonly Action confirmAction;

        public SetStartActionHandler(PathController pathController, Action confirmAction)
        {
            this.pathController = pathController;
            this.confirmAction = confirmAction;
        }

        public override void PlayerAction()
        {
            confirmAction();
        }

        public override void SetMousePosition(Vector3 position)
        {
            base.SetMousePosition(position);
            pathController.SetStartPoint(position);
        }
    }
}
