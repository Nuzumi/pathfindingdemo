using Assets.Scripts.Grid;
using Assets.Scripts.Input;
using UnityEngine;

namespace Assets.Scripts.MousePointer
{
    public class MousePointerController : MonoBehaviour
    {
        [SerializeField] private InteractionsInputController interactionsInputController;
        [SerializeField] private GridConfig GridCondig;

        private void Update()
        {
            var mousePosition = interactionsInputController.MouseWorldPosition;
            transform.position = GridCondig.GetGridPosition(mousePosition);
        }
    }
}