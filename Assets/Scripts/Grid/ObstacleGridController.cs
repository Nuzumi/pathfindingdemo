using System;
using Unity.Collections;
using UnityEngine;
using Random = Unity.Mathematics.Random;

namespace Assets.Scripts.Grid
{

    public class ObstacleGridController : MonoBehaviour
    {
        public NativeArray<bool>.ReadOnly Grid => grid.AsReadOnly();

        [SerializeField] private GridConfig gridConfig;
        [Range(0f, 1f)]
        [SerializeField] private float obstaclePercent;
        [SerializeField] private uint randomSeed;
        [SerializeField] private ObstaclePool obstaclePool;

        private NativeArray<bool> grid;
        private GameObject[] obstacles;
        private Random random;

        public void ChangeObstacleAtPosition(Vector3 position)
        {
            var index = gridConfig.GetPositionIndex(position);

            if (grid[index])
            {
                SetObstacleOnIndex(index);
            }
            else
            {
                RemoveObstacleFromIndex(index);
            }

            grid[index] = !grid[index];
        }

        private void Start()
        {
            random = new Random(randomSeed);
            grid = new NativeArray<bool>(gridConfig.CellsCount, Allocator.Persistent);
            obstacles = new GameObject[grid.Length];
            InitializeObstaclesData();
        }

        private void OnDestroy()
        {
            grid.Dispose();
        }

        private void InitializeObstaclesData()
        {
            for (int i = 0; i < grid.Length; i++)
            {
                grid[i] = random.NextFloat() > obstaclePercent;

                if (!grid[i])
                {
                    SetObstacleOnIndex(i);
                }
            }
        }

        private void SetObstacleOnIndex(int index)
        {
            if (obstacles[index] != null)
            {
                return;
            }

            var position = gridConfig.GetIndexPosition(index);
            var obstacle = obstaclePool.GetObstacle();
            obstacle.transform.position = position;
            obstacles[index] = obstacle;
        }

        private void RemoveObstacleFromIndex(int index)
        {
            if (obstacles[index] == null)
            {
                return;
            }

            var obstacle = obstacles[index];
            obstacles[index] = null;
            obstaclePool.ReturnObstacle(obstacle);
        }
    }
}