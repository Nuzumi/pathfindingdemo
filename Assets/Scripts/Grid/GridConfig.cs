﻿using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.Grid
{
    [CreateAssetMenu(menuName = "Data/" + nameof(GridConfig), fileName = nameof(GridConfig))]
    public class GridConfig : ScriptableObject
    {
        public int CellsCount => GridSize.x * GridSize.y;

        [field: SerializeField] public float CellSize { get; private set; }
        [field: SerializeField] public Vector2Int GridSize { get; private set; }

        private Vector3 GridPositionOffset => new(CellSize / 2f, 0, CellSize / 2f);

        public Vector3 GetIndexPosition(int index)
        {
            var x = index % GridSize.x;
            var z = index / GridSize.x;

            return new Vector3(x, 0, z) * CellSize + GridPositionOffset;
        }

        public int GetPositionIndex(Vector3 position)
        {
            var scaledPosition = (position / CellSize).ToIntVector();
            return scaledPosition.z * GridSize.x + scaledPosition.x;
        }

        public Vector3 GetGridPosition(Vector3 position) => GetIndexPosition(GetPositionIndex(position));

        public bool IsPositionOnGrid(Vector2Int position)
        {
            if (position.x < 0 || position.x >= GridSize.x)
            {
                return false;
            }

            if(position.y < 0 || position.y >= GridSize.y)
            {
                return false;
            }

            return true;
        }
    }
}