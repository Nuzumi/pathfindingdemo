﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Grid
{
    public class ObstaclePool : MonoBehaviour
    {
        [SerializeField] private GameObject obstaclePrefab;
        [SerializeField] private int initialPoolSize;

        private List<GameObject> obstacles;

        private void Start()
        {
            obstacles = new List<GameObject>(initialPoolSize);
            InitObstaclePool();
        }

        public GameObject GetObstacle()
        {
            if(obstacles.Count == 0)
            {
                obstacles.Add(Instantiate(obstaclePrefab, transform));
            }

            var obstacle = obstacles[0];
            obstacles.RemoveAt(0);
            obstacle.SetActive(true);
            return obstacle;
        }

        public void ReturnObstacle(GameObject obstacle)
        {
            obstacles.Add(obstacle);
            obstacle.SetActive(false);
        }

        private void InitObstaclePool()
        {
            for (int i = 0; i < initialPoolSize; i++)
            {
                var obstacle = Instantiate(obstaclePrefab, transform);
                obstacles.Add(obstacle);
                obstacle.SetActive(false);
            }
        }
    }
}